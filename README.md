# gorelease

This modules is used to query for the current stable Go releases. It queries
https://go.dev/dl/?mode=json for the current versions.

## Copyright

This is copyrighted and made freely available under the terms of the MIT
license.