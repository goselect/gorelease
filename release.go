package gorelease

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

var DefaultClient = http.DefaultClient

const releaseURL = `https://go.dev/dl/?mode=json`

// File contains information about a downloadable file in a Release.
type File struct {
	Filename string `json:"filename"`
	OS       string `json:"os"`
	Arch     string `json:"arch"`
	Version  string `json:"string"`
	SHA256   string `json:"sha256"`
	Size     int    `json:"size"`
	Kind     string `json:"kind"`
} //struct

// Release is an available Go release.
type Release struct {
	Version string `json:"version"`
	Stable  bool   `json:"stable"`
	Files   []File `json:"files"`
} //struct

// parseReleases will parse Releases from a Reader containing JSON data.
func parseReleases(r io.Reader) (releases []Release, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("parse releases: %w", err)
		} //if
	}() //func

	err = json.NewDecoder(r).Decode(&releases)
	if err != nil {
		return
	} //if

	return
} //func

// makeRequest will use the provided client to perform the provided request. It
// will parse the response for Releases.
func makeRequest(request *http.Request) (releases []Release, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("make request: %w", err)
		} //if
	}() //func

	response, err := DefaultClient.Do(request)
	if err != nil {
		return
	} //if
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		err = fmt.Errorf("bad status %s", response.Status)
		return
	} //if

	releases, err = parseReleases(response.Body)

	return
} //func

// GetReleases makes a HTTP request and parses Releases. The function accepts
// a Context and Client. Both are optional. If no Context is provided a new
// context.Background() will be created.  If no Client is provided then
// http.DefaultClient will be used.
func GetReleases(ctx context.Context) (releases []Release, err error) {
	return GetRelesasesFrom(ctx, releaseURL)
} //func

func GetRelesasesFrom(ctx context.Context, url string) (releases []Release, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("get releases: %w", err)
		} //if
	}() //func

	if ctx == nil {
		ctx = context.Background()
	} //if

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		return
	} //if

	releases, err = makeRequest(request)

	return
} //func
