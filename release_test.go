package gorelease

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

//gocyclo:ignore
func Test_parseRelease(t *testing.T) {
	t.Run("Bad", func(t *testing.T) {
		input := []string{"", "not a version", "11111"}

		for _, bad := range input {
			_, err := parseReleases(strings.NewReader(bad))
			if err != nil {
				continue
			} //if

			t.Errorf(`expected error with input "%s"`, bad)
		} //for
	}) //func
	t.Run("Good", func(t *testing.T) {
		expectedValues := []Release{
			{
				Version: "1.17.7",
				Stable:  true,
				Files: []File{
					{
						Filename: "go1.17.7.src.tar.gz",
						Version:  "1.17.7",
						SHA256:   "a",
						Size:     1,
						Kind:     "source",
					},
				},
			},
			{
				Version: "1.16.13",
				Stable:  true,
				Files: []File{
					{
						Filename: "go1.16.13.linux-amd64.tar.gz",
						Version:  "1.16.13",
						OS:       "linux",
						Arch:     "amd64",
						SHA256:   "b",
						Size:     2,
						Kind:     "archive",
					},
				},
			},
		}

		releaseJSON, err := json.Marshal(expectedValues)
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		releases, err := parseReleases(bytes.NewReader(releaseJSON))
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		if expected, actual := len(expectedValues), len(releases); actual != expected {
			t.Fatalf("expected number of releases to be %d but was %d",
				expected, actual)
		} //if

		for i := range releases {
			actualRelease := releases[i]
			expectedRelease := expectedValues[i]

			if actualRelease.Version != expectedRelease.Version {
				t.Errorf(`expected version "%s" but was"%s"`,
					expectedRelease.Version, actualRelease.Version)
			} //if

			if actualRelease.Stable != expectedRelease.Stable {
				t.Errorf(`expected stable %v but was %v`,
					expectedRelease.Stable, actualRelease.Stable)
			} //if

			if expected, actual := len(expectedRelease.Files), len(actualRelease.Files); actual != expected {
				t.Errorf("expected number of files to be %d but was %d",
					expected, actual)
				continue
			} //if

			for j := range expectedRelease.Files {
				actualFile := actualRelease.Files[j]
				expectedFile := expectedRelease.Files[j]

				if actualFile.Filename != expectedFile.Filename {
					t.Errorf(`expected filename "%s" but was "%s"`,
						actualFile.Filename, expectedFile.Filename)
				} //if

				if actualFile.OS != expectedFile.OS {
					t.Errorf(`expected os "%s" but was "%s"`,
						expectedFile.OS, actualFile.OS)
				} //if

				if actualFile.Arch != expectedFile.Arch {
					t.Errorf(`expected arch "%s" but was "%s"`,
						expectedFile.Arch, actualFile.Arch)
				} //if

				if actualFile.Version != expectedFile.Version {
					t.Errorf(`expected version "%s" but was "%s"`,
						expectedFile.Version, actualFile.Version)
				} //if

				if actualFile.SHA256 != expectedFile.SHA256 {
					t.Errorf(`expected sha256 "%s" but was "%s"`,
						expectedFile.SHA256, actualFile.SHA256)
				} //if

				if actualFile.Size != expectedFile.Size {
					t.Errorf("expected size %d but was %d",
						expectedFile.Size, actualFile.Size)
				} //if

				if actualFile.Kind != expectedFile.Kind {
					t.Errorf(`expected kind "%s" but was "%s"`,
						expectedFile.Kind, actualFile.Kind)
				} //if
			} //for
		} //for
	}) //func
} //func

func Test_makeRequest(t *testing.T) {
	t.Run("StatusNotFound", func(t *testing.T) {
		srv := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
			http.Error(rw, "Not Found", http.StatusNotFound)
		})) //func
		defer srv.Close()

		req, err := http.NewRequest(http.MethodGet, srv.URL, nil)
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		_, err = makeRequest(req)

		if err == nil {
			t.Error("Expected error for bad status")
		} //if
	}) //func
	t.Run("StatusOK", func(t *testing.T) {
		expectedValues := []Release{
			{
				Version: "1.17.7",
				Stable:  true,
				Files: []File{
					{
						Filename: "go1.17.7.src.tar.gz",
						Version:  "1.17.7",
						SHA256:   "a",
						Size:     1,
						Kind:     "source",
					},
				},
			},
			{
				Version: "1.16.13",
				Stable:  true,
				Files: []File{
					{
						Filename: "go1.16.13.linux-amd64.tar.gz",
						Version:  "1.16.13",
						OS:       "linux",
						Arch:     "amd64",
						SHA256:   "b",
						Size:     2,
						Kind:     "archive",
					},
				},
			},
		}

		srv := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
			err := json.NewEncoder(rw).Encode(expectedValues)
			if err != nil {
				t.Fatal("unexpected server error:", err)
			} //if
		})) //func
		defer srv.Close()

		req, err := http.NewRequest(http.MethodGet, srv.URL, nil)
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		releases, err := makeRequest(req)

		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		if expected, actual := len(expectedValues), len(releases); actual != expected {
			t.Fatalf("expected number of releases to be %d but was %d",
				expected, actual)
		} //if
	}) //func
} //func
